

| FIELD | NAME OF THE FIELD                   | FORMAT | CHARS | LENGTH | CODING |
|-------|-------------------------------------|--------|-------|--------|--------|
| 2     | Primary Account Number              | HLVAR  | n     | 16..19 | BCD    |
| 3     | Processing Code                     | FIXED  | n     | 6      | BCD    |
| 4     | Amount, Transaction                 | FIXED  | n     | 12     | BCD    |
| 6     | Amount, Cardholder Billing          | FIXED  | n     | 12     | BCD    |
| 7     | Transmission Date & Time            | FIXED  | n     | 10     | BCD    |
| 10    | Conversion Rate, Cardholder Billing | FIXED  | n     | 8      | BCD    |
| 11    | System Trace Audit Number           | FIXED  | n     | 6      | BCD    |
| 12    | Local Transaction Time              | FIXED  | n     | 6      | BCD    |
| 13    | Local Transaction Date              | FIXED  | n     | 4      | BCD    |
| 18    | Merchant's Type                     | FIXED  | n     | 4      | BCD    |
| 22    | Point of Service Entry Mode         | FIXED  | n     | 3      | BCD    |
| 32    | Originator Institution ID, Code     | HLVAR  | n     | 5..11  | BCD    |
| 37    | Retrieval Reference Number          | FIXED  | an    | 12     | ASCII  |
| 39    | Response Code                       | FIXED  | an    | 2      | ASCII  |
| 41    | Card Acceptor Terminal Id.          | FIXED  | ans   | 8      | ASCII  |
| 42    | Card Acceptor Identification Code   | FIXED  | ans   | 15     | ASCII  |
| 43    | Merchant Name/Location              | FIXED  | ans   | 40     | ASCII  |
| 47    | Proprietary Field                   |        |       | 47     | HLLVAR |
| 47.27 | PDS927 Counterpart Account ID       |        | ans   |        |        |
| 49    | Transaction Currency Code           | FIXED  | n     | 3      | BCD    |
| 51    | Cardholder Billing Currency Code    | FIXED  | n     | 3      | BCD    |
| 70    | Network Management Information Code | FIXED  | n     | 3      | BCD    |
| 112   | Additional Info                     | HLLVAR | an    | 999    | BIN    |
| 128   | Message Authentication Code         | FIXED  | an    | 64     | BIT    |

**Coding information**
- BCD – Binary Coded Decimals, left padding for odd length data
- ASCII – ASCII coded data
- BIN – Binary data, length specified in bytes
- BIT – Binary data, length specified in bits


