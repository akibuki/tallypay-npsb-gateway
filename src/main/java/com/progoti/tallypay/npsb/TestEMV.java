package com.progoti.tallypay.npsb;

import com.progoti.tallypay.npsb.jpos.NPSBBinaryPackager;
import com.progoti.tallypay.npsb.jpos.NPSBEmvTagType;
import org.jpos.emv.BinaryEMVTag;
import org.jpos.emv.EMVTagSequence;
import org.jpos.emv.LiteralEMVTag;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.jpos.q2.QBeanSupport;
import org.jpos.util.Logger;
import org.jpos.util.SimpleLogListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TestEMV  extends QBeanSupport {

    @Override
    protected void startService() throws Exception {
        try{
            ISOMsg msg = new ISOMsg("0600");

            msg.set(11, Integer.toString(123));
//            msg.set(12, new SimpleDateFormat("HHmmss").format(Calendar.getInstance().getTime()));
//            msg.set(13, new SimpleDateFormat("MMYY").format(Calendar.getInstance().getTime()));

            ISOMsg field112 = new ISOMsg(112);
            EMVTagSequence sequence = new EMVTagSequence();
//            sequence.add(new BinaryEMVTag(NPSBEmvTagType.MCTINFO112_F0_D2, NPSBEmvTagType.MCTINFO112_F0_D2.getTagNumber(), new byte[] {0x01, 0x02, 0x03}));
            sequence.add(new LiteralEMVTag(NPSBEmvTagType.MCTINFO112_F0_D2, NPSBEmvTagType.MCTINFO112_F0_D2.getTagNumber(), "TWHAT_TRX"));
            sequence.add(new LiteralEMVTag(NPSBEmvTagType.MCTINFO112_F0_D3, NPSBEmvTagType.MCTINFO112_F0_D3.getTagNumber(), "TQRMP"));
//            sequence.add(new LiteralEMVTag(NPSBEmvTagType.BMP55_SF99, NPSBEmvTagType.BMP55_SF99.getTagNumber(), Integer.toString(0)));
            sequence.writeTo(field112);
            msg.set(field112); // ICC data

            NPSBBinaryPackager packager = new NPSBBinaryPackager();
            Logger logger = new Logger();
            logger.addListener(new SimpleLogListener(System.err));
            packager.setLogger(logger, "bug349");
            msg.setPackager(packager);

//            byte[] out = msg.pack();
//            System.out.println("bin msg: " + out);
            msg.dump(System.out, "");
            String data = ISOUtil.hexdump(msg.pack());
            System.out.println("Dumping network data");
            System.out.println("-----------------------------------------------------");
            System.out.println(data);
            System.out.println("-----------------------------------------------------");

        } catch (ISOException e) {
            e.printStackTrace();
        }
    }

}
