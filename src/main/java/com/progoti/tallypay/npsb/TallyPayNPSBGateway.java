package com.progoti.tallypay.npsb;

import org.jpos.iso.ISOException;
import org.jpos.q2.Q2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@SpringBootApplication
public class TallyPayNPSBGateway {

	public static void main(String[] args) {
		SpringApplication.run(TallyPayNPSBGateway.class, args);

		/*Q2 q2 = new Q2 ();
		Thread t = new Thread(q2);
		t.start();*/


		TallyPayNPSBGateway tpgw = new TallyPayNPSBGateway();
		tpgw.printRequestMessage();
	}


	@GetMapping("send-request-message-to-npsb")
	public String sendRequestMessage() throws ISOException {
		try {
			new SendTestMessage().startService();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "done";
	}


	private String printRequestMessage() {
		try {
//			new GenerateTestMessage().startService();
//			new TLVTest().startService();
			new TestEMV().startService();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "done";
	}
}
