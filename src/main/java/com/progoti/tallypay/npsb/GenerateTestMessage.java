package com.progoti.tallypay.npsb;

import com.progoti.tallypay.npsb.jpos.BackupLiteralNPSBTag;
import com.progoti.tallypay.npsb.jpos.BackupNPSBStandardTagType;
import org.jpos.emv.EMVTagSequence;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOServer;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.packager.GenericPackager;
import org.jpos.q2.QBeanSupport;
import org.jpos.tlv.LiteralTagValue;
import org.jpos.tlv.TLVList;


public class GenerateTestMessage extends QBeanSupport {

    private ISOServer isoServer;
    static final TLVList.TLVListBuilder BUILDER_FT2     = TLVList.TLVListBuilder.createInstance().fixedTagSize(2);
    static final int TEST_TAG1      = 0xD2;
    static final int TEST_TAG2      = 0xD3;
    TLVList instance;

    @Override
    protected void startService() throws Exception {

//        QMUX mux = (QMUX) NameRegistrar.getIfExists("mux.tallypay-npsb-mux");
//        isoServer = ISOServer.getServer("server-1");
//        if (isoServer.getConnections() > 0 && mux != null) {
//        if (mux != null) {
            try {
//                if (mux.isConnected()) {
                    ISOMsg m = new ISOMsg();

                    m.setMTI("0100");
                    m.set(2, "1750000000000");

                    /*m.set("112.2", "TWHAT_TRX");
                    m.set("112.3", "TQRMP");*/

                    /*instance = BUILDER_FT2.build();
                    instance.append(TEST_TAG1, "TWHAT_TRX".getBytes(StandardCharsets.US_ASCII));
                    instance.append(TEST_TAG2, "TQRMP".getBytes(StandardCharsets.US_ASCII));*/

                    EMVTagSequence tvs = new EMVTagSequence();
//                    tvs.add(new LiteralEMVTag(TEST_TAG1, "TWHAT_TRX"));
//                    tvs.add(new LiteralTagValue("D2", "TWHAT_TRX"));
//                    tvs.add(new LiteralTagValue("D3", "TQRMPTQRMP"));

//                    tvs.add(new LiteralEMVTag(EMVStandardTagType.APPLICATION_PRIMARY_ACCOUNT_NUMBER_0x5A, "TWHAT_TRX"));

                    tvs.add(new BackupLiteralNPSBTag(BackupNPSBStandardTagType.D2, "TWHAT_TRX"));
                    tvs.add(new LiteralTagValue("D3", "TQRMPTQRMP"));

                    ISOMsg de112 = new ISOMsg(112);
                    tvs.writeTo(de112);
                    m.set(de112);



//                    m.set("47.927", "01234567890");
//                    m.set(49, "050");

                    /*GenericTagSequence gts = new GenericTagSequence();
                    gts.add(new LiteralTagValue("927", "01912203602"));

                    ISOMsg de47 = new ISOMsg(47);
                    gts.writeTo(de47);
                    m.set(de47);*/

                    /*
                    m.set(2, "0001750000000000");
//                    m.set(2, "1750000000000");
                    m.set(3, "280000");
                    m.set(4, "000000010000");
                    m.set(6, "000000010000");
                    m.set(7, "0929045408");
                    m.set(11, "496719");
                    m.set(12, "105408");
                    m.set(13, "0929");
                    m.set(18, "4111");
                    m.set(22, "000");
                    m.set(32, "000125");
                    m.set(37, "127210496719");
                    m.set(41, "IBNK0000");
                    m.set(42, "10039004       ");
                    m.set(43, "IBBL Online UAT          Dhaka        BD");
//                    m.set(47, "92701101912203602");
//                    m.set("47.27", "01912203602");
                    m.set(49, "050");
                    m.set(51, "050");
                    m.set(112, ISOUtil.hex2byte("F012D20954574841545F545258D3055451524D50"));
*/
                    ISOMsg mc = (ISOMsg) m.clone();
                    mc.setPackager(new GenericPackager("cfg/packager/npsb_packager.xml"));
//                    mc.setPackager(new GenericPackager("cfg/packager/bangla_qr_packager.xml"));
                    String data = ISOUtil.hexdump(mc.pack());
                    System.out.println("Dumping network data");
                    System.out.println("-----------------------------------------------------");
                    System.out.println(data);
                    System.out.println("-----------------------------------------------------");
//                }

            } catch (ISOException e) {
                e.printStackTrace();
            }
//        }
    }

}