package com.progoti.tallypay.npsb;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOUtil;
import org.jpos.q2.QBeanSupport;
import org.jpos.tlv.TLVList;
import org.jpos.tlv.TLVMsg;

import java.util.List;

public class TLVTest extends QBeanSupport {

    @Override
    protected void startService() throws Exception {
        try{
            String fieldValue = "F08199D20954574841545F545258D3055451524D50E08184C00E5439383734353133353438373938C10D54303831333936383737323832C20B54544849532053544F5245C3085431323334353637C40B5432313132323131373032C50954414152414448414BC609543132323337393430C70A54464F4F442042494C4CC803543132C912544247443132333435363738393039383737CA0454303030";
//            String fieldValue = "D20954574841545F545258D3055451524D50E08184C00E5439383734353133353438373938C10D54303831333936383737323832C20B54544849532053544F5245C3085431323334353637C40B5432313132323131373032C50954414152414448414BC609543132323337393430C70A54464F4F442042494C4CC803543132C912544247443132333435363738393039383737CA0454303030";
//            String fieldValue = "C00E5439383734353133353438373938C10D54303831333936383737323832C20B54544849532053544F5245C3085431323334353637C40B5432313132323131373032C50954414152414448414BC609543132323337393430C70A54464F4F442042494C4CC803543132C912544247443132333435363738393039383737CA0454303030";

            TLVList list = new TLVList();
            list.unpack(ISOUtil.hex2byte(fieldValue));
            list.dump(System.out, "");
//            System.out.println(ISOUtil.hex2byte("54303030"));

            System.out.println("Printing ......");
            List<TLVMsg> tags = list.getTags();
            for (TLVMsg tlv : tags) {
                System.out.println(tlv.getStringValue());
//                System.out.println(tlv.);
            }

            /*for (TLVMsg tlv : tags) {
                lgr.debug("Value of FieldID-{}, Tag [{}], Value [{}] is parsed as: ", () -> fieldId, () -> tlv.getTag(), () -> ISOUtil.byte2hex(tlv.getValue()));
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
