package com.progoti.tallypay.npsb;

import org.jpos.iso.*;
import org.jpos.iso.packager.GenericPackager;
import org.jpos.q2.QBeanSupport;
import org.jpos.q2.iso.QMUX;
import org.jpos.tlv.GenericTagSequence;
import org.jpos.tlv.LiteralTagValue;
import org.jpos.util.NameRegistrar;


public class SendTestMessage extends QBeanSupport {

    private ISOServer isoServer;

    @Override
    protected void startService() throws Exception {

        QMUX mux = (QMUX) NameRegistrar.getIfExists("mux.tallypay-npsb-mux");
//        isoServer = ISOServer.getServer("server-1");
//        if (isoServer.getConnections() > 0 && mux != null) {
//        if (mux != null) {
            try {
//                if (mux.isConnected()) {
                    ISOMsg m = new ISOMsg();
                    m.setMTI("0100");
                    m.set(2, "0001750000000000");
                    m.set(3, "280000");
                    m.set(4, "000000010000");
                    m.set(7, "0929045408");
                    m.set(11, "496719");
                    m.set(12, "105408");
                    m.set(13, "0929");
                    m.set(18, "4111");
                    m.set(22, "000");
                    m.set(32, "000125");
                    m.set(37, "127210496719");
                    m.set(41, "IBNK0000");
//                    m.set(42, "10039004       ");
                    m.set(42, "10039004");
                    m.set(43, "IBBL Online UAT          Dhaka        BD");

//                    m.set(47, "92701101912203602");
//                    m.set("47.27", "01912203602");
                    m.set("47.927", "01912203602");

                    /*GenericTagSequence gts = new GenericTagSequence();
                    gts.add(new LiteralTagValue("927", "01912203602"));

                    ISOMsg de47 = new ISOMsg(47);
                    gts.writeTo(de47);
                    m.set(de47);*/

                    m.set(49, "050");
//                    m.set(103, "3555101142997");
                    m.set(112, ISOUtil.hex2byte("F012D20954574841545F545258D3055451524D50"));

                    ISOMsg mc = (ISOMsg) m.clone();
                    mc.setPackager(new GenericPackager("cfg/packager/npsb_packager.xml"));
                    String data = ISOUtil.hexdump(mc.pack());
                    System.out.println("Dumping network data");
                    System.out.println("-----------------------------------------------------");
                    System.out.println(data);
                    System.out.println("-----------------------------------------------------");

                    ISOMsg response = mux.request(m, 5000);
//                }

            } catch (ISOException e) {
                e.printStackTrace();
            }
//        }
    }

}