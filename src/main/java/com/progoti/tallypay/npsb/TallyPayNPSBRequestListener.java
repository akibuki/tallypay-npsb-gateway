package com.progoti.tallypay.npsb;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;

import java.io.IOException;
import java.util.Random;

public class TallyPayNPSBRequestListener implements ISORequestListener {

    @Override
    public boolean process(ISOSource isoSource, ISOMsg isoMsg) {
        try {
            if (isoMsg.isRequest()) {
                Random random = new Random(System.currentTimeMillis());
                boolean isOdd = (Math.abs(random.nextInt()) % 2) == 1;

                isoMsg.setResponseMTI();
                isoMsg.set(39, isOdd ? "01" : "00");
                isoSource.send(isoMsg);
                return true;
            } else {
                return false;
            }
        } catch (ISOException | IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}