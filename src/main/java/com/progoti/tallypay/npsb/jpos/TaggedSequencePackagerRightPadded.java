package com.progoti.tallypay.npsb.jpos;

import java.io.ByteArrayOutputStream;

import org.jpos.iso.*;
import org.jpos.tlv.packager.TaggedSequencePackager;
import org.jpos.tlv.ISOTaggedField;
import org.jpos.tlv.OffsetIndexedComposite;
import org.jpos.util.LogEvent;
import org.jpos.util.Logger;
import org.xml.sax.Attributes;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Vishnu Pillai
 */
public class TaggedSequencePackagerRightPadded extends TaggedSequencePackager {

    private static final RightPadder ZERO_PADDER = new RightPadder('0');

    public TaggedSequencePackagerRightPadded() throws ISOException {
        super();
    }

    /**
     * Pack the subfield into a byte array
     */
    @Override
    public byte[] pack(ISOComponent m) throws ISOException {
        LogEvent evt = new LogEvent(this, "pack");
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(100)) {
            ISOComponent c;
            Map fields = m.getChildren();
            fields.remove(-1);
            int len = 0;
            boolean tagsStarted = false;
            Iterator iterator = fields.values().iterator();
            if (m instanceof OffsetIndexedComposite) {
                int offset = ((OffsetIndexedComposite) m).getOffset();
                for (int i = 0; i < offset && iterator.hasNext(); i++) {
                    iterator.next();
                }
            }
            while (iterator.hasNext() && len < this.length) {

                Object obj = iterator.next();
                c = (ISOComponent) obj;

                byte[] b;
                if (c.getValue() != null) {
                    if (c instanceof ISOTaggedField) {
                        tagsStarted = true;
                        String tag = ((ISOTaggedField) c).getTag();
                        if (tag == null) {
                            evt.addMessage("error packing subfield " + c.getKey());
                            evt.addMessage(c);
                            throw new ISOException("Tag should not be null");
                        } else {
                            ISOFieldPackager fieldPackager = (ISOFieldPackager) packagerMap.get(tag);
                            if (fieldPackager == null) {
                                fieldPackager = (ISOFieldPackager) packagerMap.get("default");
                            }
                            if (fieldPackager == null) {
                                throw new ISOException("No default tag packager and no field packager configured for tag: " + tag);
                            }
                            b = fieldPackager.pack(c);
                            if (len + b.length > this.length) {
                                break;
                            }
                            len += b.length;
                            bout.write(b);
                        }
                    } else if (!tagsStarted && fld.length > (Integer) c.getKey() && fld[(Integer) c.getKey()] != null) {
                        b = fld[(Integer) c.getKey()].pack(c);
                        len += b.length;
                        bout.write(b);
                    } else {
                        int tagNumber = (Integer) c.getKey();
                        String tag = ISOUtil.padright(String.valueOf(tagNumber), this.tag.length(), '0');
                        ISOTaggedField isoTaggedField = new ISOTaggedField(tag, c);
                        if (fld.length > tagNumber) {
                            b = fld[(Integer) c.getKey()].pack(isoTaggedField);
                        } else {
                            ISOFieldPackager fieldPackager = (ISOFieldPackager) packagerMap.get(tag);
                            if (fieldPackager == null) {
                                fieldPackager = (ISOFieldPackager) packagerMap.get("default");
                            }
                            if (fieldPackager == null) {
                                throw new ISOException("No default tag packager and no field packager configured for tag: " + tag);
                            }
                            b = fieldPackager.pack(isoTaggedField);
                            if (len + b.length > this.length) {
                                break;
                            }
                        }
                        len += b.length;
                        bout.write(b);
                    }
                }
                if (m instanceof OffsetIndexedComposite) {
                    ((OffsetIndexedComposite) m).incOffset();
                }
            }

            byte[] d = bout.toByteArray();
            if (logger != null)  // save a few CPU cycle if no logger available
                evt.addMessage(ISOUtil.hexString(d));
            return d;
        } catch (ISOException e) {
            evt.addMessage(e);
            throw e;
        } catch (Exception e) {
            evt.addMessage(e);
            throw new ISOException(e);
        } finally {
            Logger.log(evt);
        }
    }

    protected ISOFieldPackager getTagPackager() {
        IF_CHAR tagPackager = new IF_CHAR(this.tag.length(), "Tag");
        tagPackager.setPadder(ZERO_PADDER);
        return tagPackager;
    }
}