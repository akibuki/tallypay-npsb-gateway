package com.progoti.tallypay.npsb.jpos;

public class BackupLiteralNPSBTag extends BackupNPSBTag<String> {

    public BackupLiteralNPSBTag(BackupNPSBStandardTagType tagType, String value)
            throws IllegalArgumentException {
        super(tagType, value);
    }

}