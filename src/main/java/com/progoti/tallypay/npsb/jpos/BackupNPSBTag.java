package com.progoti.tallypay.npsb.jpos;

import org.jpos.emv.ProprietaryFormatException;
import org.jpos.tlv.TLVDataFormat;
import org.jpos.tlv.TagValue;

import java.io.Serializable;


public abstract class BackupNPSBTag<T> implements TagValue<T>, Serializable {

    private static final long serialVersionUID = 4674858246785118616L;
    private final BackupNPSBTagType tagType;
    private final TLVDataFormat dataFormat;
    private final Integer tagNumber;
    private final T value;


    public BackupNPSBTag(final BackupNPSBStandardTagType tagType, final T value) throws IllegalArgumentException {
        if (tagType == null) {
            throw new IllegalArgumentException("tagType cannot be null");
        }
        if (value == null) {
            throw new IllegalArgumentException("value cannot be null");
        }
        if (tagType.isProprietaryTag()) {
            throw new IllegalArgumentException("Tag Number must be specified for proprietary tag");
        }
        if (tagType.isProprietaryFormat()) {
            throw new IllegalArgumentException(
                    "Tag TLVDataFormat must be specified for tag with proprietary format");
        } else {
            try {
                if (!tagType.getDataType().isAssignableFrom(value.getClass())) {
                    throw new IllegalArgumentException("Tag: " + tagType.getTagNumberHex() +
                            ". Value should be of type: " + tagType.getDataType() + ". Received " +
                            value.getClass());
                }
            } catch (ProprietaryFormatException e) {
                throw new IllegalStateException(e);
            }
        }
        this.tagType = tagType;
        this.value = value;
        this.tagNumber = tagType.getTagNumber();
        this.dataFormat = tagType.getFormat();
    }


    private static Class<?> getDataType(TLVDataFormat dataFormat)
            throws IllegalArgumentException {
        switch (dataFormat) {
            case BINARY:
            case CONSTRUCTED:
            case PROPRIETARY:
                return byte[].class;
            default:
                return String.class;
        }
    }

    @Override
    public String getTag() {
        return getTagNumberHex();
    }

    @Override
    public boolean isComposite() {
        return false;
    }

    public TLVDataFormat getDataFormat() {
        return dataFormat;
    }

    public Integer getTagNumber() {
        return tagNumber;
    }

    public BackupNPSBTagType getTagType() {
        return tagType;
    }

    public T getValue() {
        return value;
    }

    public String getTagNumberHex() {
        return Integer.toHexString(tagNumber).toUpperCase();
    }
}
