package com.progoti.tallypay.npsb.jpos;

import org.jpos.emv.ProprietaryFormatException;
import org.jpos.tlv.TLVDataFormat;

/**
 * @author Vishnu Pillai
 */
public interface BackupNPSBTagType {

    int getTagNumber();

    String getTagShortDescription();

    String getTagDescription();

    DataSource getSource();

    TLVDataFormat getFormat();

    DataLength getDataLength();

    ByteLength getByteLength();

    boolean isProprietaryFormat();

    String getTagNumberHex();

    byte[] getTagNumberBytes();

    Class<?> getDataType() throws ProprietaryFormatException;

    boolean isProprietaryTag();

    enum DataSource {
        ICC,
        TERMINAL,
        ISSUER
    }

    abstract class DataLength {

        public static final int DATA_LENGTH_VAR = -2;
        public static final int DATA_LENGTH_PROPRIETARY = -1;
        private int minLength;
        private int maxLength;


        public DataLength(int minLength, int maxLength) {
            this.minLength = minLength;
            this.maxLength = maxLength;
        }


        public DataLength(int minLength) {
            this.minLength = minLength;
        }

        public int getMinLength() {
            return minLength;
        }

        public void setMinLength(int minLength) {
            this.minLength = minLength;
        }

        public int getMaxLength() {
            return maxLength;
        }

        public void setMaxLength(int maxLength) {
            this.maxLength = maxLength;
        }

        public abstract boolean isFixedLength();
    }

    class ProprietaryDataLength extends DataLength {

        public ProprietaryDataLength() {
            super(-1, -1);
        }

        @Override
        public int getMinLength() {
            return super.getMinLength();
        }

        @Override
        public void setMinLength(final int minLength) {
            super.setMinLength(minLength);
        }

        @Override
        public int getMaxLength() {
            return super.getMaxLength();
        }

        @Override
        public void setMaxLength(final int maxLength) {
            super.setMaxLength(maxLength);
        }

        @Override
        public boolean isFixedLength() {
            return false;
        }
    }

    class FixedDataLength extends DataLength {

        public FixedDataLength(int length) {
            super(length, length);
        }

        @Override
        public boolean isFixedLength() {
            return true;
        }
    }

    class VariableDataLength extends DataLength {

        public VariableDataLength(int minLength, int maxLength) {
            super(minLength, maxLength);
        }

        @Override
        public boolean isFixedLength() {
            return false;
        }
    }

    class VariableDiscreteDataLength extends DataLength {

        public VariableDiscreteDataLength(int minLength, int maxLength) {
            super(minLength, maxLength);
        }

        @Override
        public boolean isFixedLength() {
            return true;
        }
    }

    abstract class ByteLength {

        public static final int BYTE_LENGTH_PROPRIETARY = -1;
        public static final int BYTE_LENGTH_VAR = -2;
        private int minLength;
        private int maxLength;


        public ByteLength(int minLength, int maxLength) {
            this.minLength = minLength;
            this.maxLength = maxLength;
        }


        public ByteLength(int minLength) {
            this.minLength = minLength;
        }

        public int getMinLength() {
            return minLength;
        }

        public void setMinLength(int minLength) {
            this.minLength = minLength;
        }

        public int getMaxLength() {
            return maxLength;
        }

        public void setMaxLength(int maxLength) {
            this.maxLength = maxLength;
        }

        public abstract boolean isFixedLength();
    }

    class VariableDiscreteByteLength extends ByteLength {

        public VariableDiscreteByteLength(int minLength, int maxLength) {
            super(minLength, maxLength);
        }

        @Override
        public boolean isFixedLength() {
            return true;
        }
    }

    class FixedByteLength extends ByteLength {

        public FixedByteLength(int length) {
            super(length, length);
        }

        @Override
        public boolean isFixedLength() {
            return true;
        }
    }

    class VariableByteLength extends ByteLength {

        public VariableByteLength(int minLength, int maxLength) {
            super(minLength, maxLength);
        }

        @Override
        public boolean isFixedLength() {
            return false;
        }
    }
}