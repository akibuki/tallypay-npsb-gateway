/*
 * jPOS Project [http://jpos.org]
 * Copyright (C) 2000-2023 jPOS Software SRL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.progoti.tallypay.npsb.jpos;

import org.jpos.emv.*;
import org.jpos.iso.ISOUtil;
import org.jpos.tlv.TLVDataFormat;

import java.util.HashMap;
import java.util.Map;

public enum NPSBEmvTagType implements EMVProprietaryTagType {

    MCTINFO112_F0(0xF0, "Terminal Capabilities",
            "Indicates the card data input, CVM, and security capabilities of the terminal",
            DataSource.TERMINAL, TLVDataFormat.BINARY, new VariableDataLength(0, 999), 0x00, new FixedByteLength(3)),

    MCTINFO112_F0_D2(0xD2, "Terminal Capabilities",
            "Indicates the card data input, CVM, and security capabilities of the terminal",
            DataSource.TERMINAL, TLVDataFormat.ASCII_ALPHA_NUMERIC_SPACE, new FixedDataLength(9), 0x00, new FixedByteLength(3)),

    MCTINFO112_F0_D3(0xD3, "Terminal Capabilities",
            "Indicates the card data input, CVM, and security capabilities of the terminal",
            DataSource.TERMINAL, TLVDataFormat.ASCII_ALPHA, new FixedDataLength(5), 0x00, new FixedByteLength(3)),

    BMP55_SF99(99, "Message Control field",
            "Message control field for terminal configuration", DataSource.TERMINAL,
            TLVDataFormat.ASCII_NUMERIC, new FixedDataLength(2), 0x00, new FixedByteLength(2));

    private final int tagNumber;
    private final String tagShortDescription;
    private final String tagDescription;
    private final DataSource source;
    private final TLVDataFormat format;
    private final Integer template;
    private final DataLength dataLength;
    private final ByteLength byteLength;

    NPSBEmvTagType(final int tagNumber, final String tagName, final String tagDescription,
                   final DataSource source, final TLVDataFormat format, final DataLength dataLength,
                   final Integer template, final ByteLength byteLength) {
        this.tagNumber = tagNumber;
        this.tagShortDescription = tagName;
        this.tagDescription = tagDescription;
        this.source = source;
        this.format = format;
        this.template = template;
        this.dataLength = dataLength;
        this.byteLength = byteLength;
        if (!(0 == tagNumber)) {
            if (MapHolder.tagCodeMap.containsKey(tagNumber)) {
                throw new IllegalStateException(
                        "Illegal attempt to add duplicate EMVTagType with tagNumber: " + tagNumber +
                                ". Enum: " + this.name());
            }
            MapHolder.tagCodeMap.put(tagNumber, this);
        }
    }

    public static boolean isProprietaryTag(int code) {
        EMVTagType tagType = MapHolder.tagCodeMap.get(code);
        return tagType == null;
    }

    public static NPSBEmvTagType forCode(int code) throws UnknownTagNumberException {
        NPSBEmvTagType tagType = MapHolder.tagCodeMap.get(code);
        if (tagType == null) {
            throw new UnknownTagNumberException(String.valueOf(code));
        }
        return tagType;
    }

    public static NPSBEmvTagType forHexCode(String hexString) throws UnknownTagNumberException {
        return forCode(Integer.parseInt(hexString, 16));
    }

    @Override
    public int getTagNumber() {
        return tagNumber;
    }

    public boolean isProprietaryTag() {
        return false;
    }

    public int getTagNumberLength() {
        return tagNumber > 0xFF ? 2 : 1;
    }

    public String getTagNumberHex() {
        return Integer.toHexString(tagNumber).toUpperCase();
    }

    public byte[] getTagNumberBytes() {
        return ISOUtil.int2byte(tagNumber);
    }

    @Override
    public String getTagShortDescription() {
        return tagShortDescription;
    }

    @Override
    public String getTagDescription() {
        return tagDescription;
    }

    @Override
    public DataSource getSource() {
        return source;
    }

    @Override
    public TLVDataFormat getFormat() {
        return format;
    }

    public boolean isProprietaryFormat() {
        return TLVDataFormat.PROPRIETARY.equals(format);
    }

    /**
     * @return The template or null if no template
     */
    public EMVTagType getTemplate() {
        return MapHolder.tagCodeMap.get(this.template);
    }

    @Override
    public DataLength getDataLength() {
        return dataLength;
    }

    @Override
    public ByteLength getByteLength() {
        return byteLength;
    }

    public Class<?> getDataType() throws ProprietaryFormatException {
        switch (format) {
            case PROPRIETARY:
                throw new ProprietaryFormatException(tagShortDescription);
            case BINARY:
                return byte[].class;
            case CONSTRUCTED:
                return EMVTag[].class;
            default:
                return String.class;
        }
    }

    private static class MapHolder {

        private static Map<Integer, NPSBEmvTagType> tagCodeMap =
                new HashMap<Integer, NPSBEmvTagType>();
    }

    public static class ProprietaryFixedDataLength extends DataLength {

        public static final DataLength INSTANCE = new ProprietaryFixedDataLength(-1);


        public ProprietaryFixedDataLength(int length) {
            super(length, length);
        }

        @Override
        public boolean isFixedLength() {
            return true;
        }
    }

    public static class ProprietaryVariableDataLength extends DataLength {

        public static final DataLength INSTANCE = new ProprietaryVariableDataLength(-1, -1);


        public ProprietaryVariableDataLength(int minLength, int maxLength) {
            super(minLength, maxLength);
        }

        @Override
        public boolean isFixedLength() {
            return false;
        }
    }

    public static class ProprietaryVariableDiscreteDataLength extends DataLength {

        public static final DataLength INSTANCE = new ProprietaryVariableDiscreteDataLength(-1, -1);


        public ProprietaryVariableDiscreteDataLength(int minLength, int maxLength) {
            super(minLength, maxLength);
        }

        @Override
        public boolean isFixedLength() {
            return true;
        }
    }

    public static class ProprietaryVariableDiscreteByteLength extends ByteLength {

        public static final ByteLength INSTANCE = new ProprietaryVariableDiscreteByteLength(-1, -1);


        public ProprietaryVariableDiscreteByteLength(int minLength, int maxLength) {
            super(minLength, maxLength);
        }

        @Override
        public boolean isFixedLength() {
            return true;
        }
    }

    public static class ProprietaryFixedByteLength extends ByteLength {

        public static final ByteLength INSTANCE = new ProprietaryFixedByteLength(-1);


        public ProprietaryFixedByteLength(int length) {
            super(length, length);
        }

        @Override
        public boolean isFixedLength() {
            return true;
        }
    }

    public static class ProprietaryVariableByteLength extends ByteLength {

        public static final ByteLength INSTANCE = new ProprietaryVariableByteLength(-1, -1);


        public ProprietaryVariableByteLength(int minLength, int maxLength) {
            super(minLength, maxLength);
        }

        @Override
        public boolean isFixedLength() {
            return false;
        }
    }
}
