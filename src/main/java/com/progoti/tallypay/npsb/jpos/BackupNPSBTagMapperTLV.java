package com.progoti.tallypay.npsb.jpos;

import org.jpos.iso.packager.TagMapper;

import java.util.HashMap;
import java.util.Map;


public class BackupNPSBTagMapperTLV implements TagMapper {

    private static final Map<String, Integer> MAP_TAG_MUMBER = new HashMap<>();
    private static final Map<String, String> MAP_NUMBER_TAG = new HashMap<>();

    static {
        MAP_TAG_MUMBER.put("112.F0", 1);
        MAP_TAG_MUMBER.put("1.D2", 2);
        MAP_TAG_MUMBER.put("1.D3", 3);

        MAP_NUMBER_TAG.put("112.1", "F0");
        MAP_NUMBER_TAG.put("1.2", "D2");
        MAP_NUMBER_TAG.put("1.3", "D3");
    }

    @Override
    public String getTagForField(int fieldNumber, int subFieldNumber) {
        return MAP_NUMBER_TAG.get(fieldNumber + "." + subFieldNumber);
    }

    @Override
    public Integer getFieldNumberForTag(int fieldNumber, String tag) {
        return MAP_TAG_MUMBER.get(fieldNumber + "." + tag);
    }
}
